package org.example;

import io.swagger.models.*;
import io.swagger.models.parameters.Parameter;
import io.swagger.parser.SwaggerParser;
import org.example.model.Paths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwaggerParserExample {
    Map<String, List<Paths>> map = new HashMap<>();
    public Map<String, List<Paths>> parse(){
        Swagger swagger = new SwaggerParser().read("C:/dev/study/java/maven-archetype-automation/src/main/resources/swagger.json");
        Map<String, Path> paths = swagger.getPaths();
        for (Map.Entry<String, Path> p : paths.entrySet()) {
            Path path = p.getValue();
            Map<HttpMethod, Operation> operations = path.getOperationMap();
            for (Map.Entry<HttpMethod, Operation> o : operations.entrySet()) {
                putPaths(p, o);
                System.out.println("===");
                System.out.println("PATH:" + p.getKey());
                System.out.println("Http method:" + o.getKey());
                System.out.println("Operation ID:" + o.getValue().getOperationId());
                System.out.println("Summary:" + o.getValue().getSummary());
                System.out.println("Parameters number: " + o.getValue().getParameters().size());
                for (Parameter parameter : o.getValue().getParameters()) {
                    System.out.println(" - " + parameter.getName());
                }
                System.out.println("Responses:");
                for (Map.Entry<String, Response> r : o.getValue().getResponses().entrySet()) {
                    System.out.println(" - " + r.getKey() + ": " + r.getValue().getDescription());
                }
                System.out.println("");
            }
        }
        System.out.println("map " + map);
        return map;
    }

    private void putPaths(Map.Entry<String, Path> p, Map.Entry<HttpMethod, Operation> o){
        Paths examplePath = new Paths();
        examplePath.setMethod(o.getKey().name());
        examplePath.setPath(p.getKey());
        examplePath.setOperationId(o.getValue().getOperationId());
        if(map.get(o.getKey().name()) != null){
            List<Paths> list = map.get(o.getKey().name());
            list.add(examplePath);
            map.put(o.getKey().name(), list);
        }else{
            List<Paths> list = new ArrayList<>();
            list.add(examplePath);
            map.put(o.getKey().name(), list);
        }
    }
}
