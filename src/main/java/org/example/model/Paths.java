package org.example.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Paths {
    private String path;
    private String method;
    private String operationId;

    public String getChannelId(){
        return "http-" + operationId +"Channel";
    }
}
