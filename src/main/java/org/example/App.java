package org.example;

import org.example.model.Paths;

import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        SwaggerParserExample parser = new SwaggerParserExample();
        Map<String, List<Paths>> map = parser.parse();

        VelocityUtil velocityUtil = new VelocityUtil();

        map.forEach((k, v) -> {
            velocityUtil.doMapping();
        });
    }
}
